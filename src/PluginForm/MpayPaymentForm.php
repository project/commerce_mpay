<?php

namespace Drupal\commerce_mpay\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MpayPaymentForm.
 */
class MpayPaymentForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * MpayPaymentForm constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The log channel factory.
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_channel_factory, ClientInterface $client) {
    $this->logger = $logger_channel_factory->get('commerce_mpay');
    $this->httpClient = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $order = $payment->getOrder();

    $mpay_data = $order->getData('mpay_payment_info');
    if (empty($mpay_data)) {
      $options = [
        'headers' => [
          'Authorization' => $payment_gateway_plugin->getConfiguration()['token'],
          'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'body' => UrlHelper::buildQuery([
          'title' => $order->getStore()->getName(),
          'currency' => $payment->getAmount()->getCurrencyCode(),
          'value' => $payment->getAmount()->getNumber(),
          'return_url' => $form['#return_url'],
          'webhook' => $payment_gateway_plugin->getNotifyUrl()->toString(),
          'key' => $order->id(),
        ]),
      ];

      try {
        $response = $this->httpClient->post('https://mpay.ms/api/payment/new', $options);

        if ($response->getStatusCode() === 200) {
          $mpay_data = Json::decode($response->getBody()->getContents());

          if ($mpay_data['status'] !== 1) {
            $this->logger->error('We got an error from MPay. Error code: @code. Error message: @message.', [
              '@code' => $mpay_data['status'],
              '@message' => $mpay_data['error'],
            ]);
            throw new PaymentGatewayException("Couldn't get information from MPay service. Please try again later.");
          }

          $order->setData('mpay_payment_info', $mpay_data);
          $order->save();

          // Save payment with default status. Will be marked as "Completed" on
          // successful IPN request.
          $payment->setRemoteId($mpay_data['id']);
          $payment->save();
        }
      }
      catch (ServerException $e) {
        $this->logger->error($e->getResponse()->getBody()->getContents());
      }
    }

    if (empty($mpay_data['url'])) {
      throw new PaymentGatewayException("Couldn't get information from MPay service. Please try again later.");
    }

    return $this->buildRedirectForm($form, $form_state, $mpay_data['url'], [], self::REDIRECT_GET);
  }

}
